<?php

namespace App\Classes;

use Carbon\Carbon;


/**
 * пробный класс форматирования даты в зависимости от локали
 * наглым образом сперт у преподавателя
 *
 * Class FormatDate
 * @package App\Classes
 */
class FormatDate
{
    /**
     * Возвращает отформатированную дату в зависимости от локали
     * строка с форматом хранится в lang файле datetume
     *
     * @param     $dateTime
     * @param int $offset
     * @return false|string
     */
    public function getFormattedDate($dateTime, $offset = 0)
    {
        $monthArray = __('datetime.months');
        $daysArray = __('datetime.days');

        $timestamp = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime)->timestamp;
        $timestamp += 3600 * $offset;

        $format = __('datetime.dateformat');

        $findArray = [
            '/%MONTH%/i',
            '/%DAYWEEK%/i'
        ];

        $replaceArray = [
            $monthArray[date("m", $timestamp) - 1],
            $daysArray[date("w", $timestamp)]
        ];

        $format = preg_replace($findArray, $replaceArray, $format);

        return date($format, $timestamp);
    }
}