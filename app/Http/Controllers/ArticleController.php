<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateArticle;
use App\Http\Requests\CreateArticlePost;
use App\Http\Requests\DeleteArticle;
use App\Http\Requests\ShowArticle;
use App\Http\Requests\UpdateArticle;
use App\Http\Requests\UpdateArticlePost;
use App\Models\Article;
use App\Repositories\RedisArticleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;


/**
 * Articles Controller
 * По сути контроллер страниц нашего блога
 *
 * Class ArticleController
 * @package App\Http\Controllers
 */
class ArticleController extends Controller
{
    /**
     * поле для нашего репозитория
     *
     * @var RedisArticleRepository
     */
    protected $articles;

    /**
     * внедряем репозиторий статьи
     * с кешированием
     *
     * ArticleController constructor.
     * @param RedisArticleRepository $articles
     */
    public function __construct(RedisArticleRepository $articles)
    {
        $this->articles = $articles;
    }

    /**
     * главная страница блога
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = $this->articles->getAllPaginate();

        return view('pages.index', compact('articles'));
    }

    /**
     * страничка для создания новой статьи
     * доступ проверяется с помощью form request и artilce policy
     *
     * @param CreateArticle $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(CreateArticle $request)
    {
        return view('pages.create');
    }

    /**
     * попытка сохранения новой статьи
     * доступ проверяется с помощью form request и artilce policy
     *
     * @param CreateArticlePost $request
     * @return string
     */
    public function createPost(CreateArticlePost $request, Article $article)
    {
        $article->user_id = Auth::user()->id;
        $article->fill($request->all());
        $id = $this->articles->add($article);

        return redirect()->route('blog.post', compact('id'));
    }

    /**
     * страница одной статьи
     *
     * @param             $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $article = $this->articles->getById($id);

        return view('pages.show', compact('article', 'id'));
    }

    /**
     * страница редактирования статьи
     * доступ проверяется с помощью form request и artilce policy
     *
     * @param UpdateArticle $request
     * @param               $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(UpdateArticle $request, $id)
    {
        //todo явный повтор метода show, подумать над этим
        $article = $this->articles->getById($id);

        return view('pages.edit', compact('article', 'id'));
    }

    /**
     * попытка сохранения отредактированной статьи
     * доступ проверяется с помощью form request и artilce policy
     *
     * @param UpdateArticlePost $request
     * @param         $id
     * @return string
     */
    public function editPost(UpdateArticlePost $request, $id)
    {
        $article = $this->articles->getById($id);

        $article->user_id = Auth::user()->id;
        $article->fill($request->all());
        $this->articles->update($article);

        return redirect()->route('blog.post', compact('id'));
    }

    /**
     * удаление статьи
     * доступ проверяется с помощью form request и artilce policy
     *
     * @param DeleteArticle $request
     * @param               $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(DeleteArticle $request, $id)
    {
        $this->articles->delete($id);

        return redirect()->route('blog.main')
            ->with('message', 'something removed');
    }
}
