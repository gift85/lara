<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterUserPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


/**
 * Базовая регистрация и аутентификация, все здесь
 *
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * Просто вьюшка логина
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('pages.login');
    }

    /**
     * Попытка логина
     * с неудачей шлем обратно
     * с успехом - на главную.
     * Валидация не требуется
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginPost(Request $request)
    {
        $remember = (bool) $request->input('remember');

        $authResult = Auth::attempt([
            'name' => $request->name,
            'password' => $request->password,
        ], $remember);

        if($authResult){
            return redirect()->route('blog.main');
        }else{
            return redirect()->back()->with('authError', 'blog.logerror');
        }
    }

    /**
     * Разлогин с последующими
     * чисткой сессии и
     * редиректом на главную
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('blog.main');
    }

    /**
     * Страничка регистрации
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('pages.register');
    }

    /**
     * Попытка регистрации.
     * Пароль хэшируется автоматически при сохранении как задано в модели User
     * В случае успеха вернет на главную уже залогиненым
     *
     * @param RegisterUserPost $request
     * @param User             $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerPost(RegisterUserPost $request, User $user)
    {
        $user->fill($request->all())
            ->save();

        Auth::login($user);

        return redirect()->route('blog.main');
    }
}
