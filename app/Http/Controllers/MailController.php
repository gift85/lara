<?php

namespace App\Http\Controllers;

use App\Events\GotFeedback;
use App\Http\Requests\FeedbackPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


/**
 * простой mail класс
 *
 * Class MailController
 * @package App\Http\Controllers
 */
class MailController extends Controller
{
    /**
     * примитивная форма обратной связи
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function feedback()
    {
        return view('pages.feedback');
    }

    /**
     * обработка и отправка сообщения от пользователя
     * затем редирект на главную
     *
     * @param FeedbackPost $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function feedbackPost(FeedbackPost $request)
    {
        event(new GotFeedback(Auth::user()->email, $request['title'], $request['content']));

        return redirect()->route('blog.main');
    }
}
