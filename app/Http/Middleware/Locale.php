<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;


/**
 * Установка языка приложения для каждого запроса
 * происходит здесь
 *
 * Class Locale
 * @package App\Http\Middleware
 */
class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('locale')){
            session('locale', config('app.locale'));
        }

        App::setLocale(session('locale'));

        return $next($request);
    }
}
