<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;


/**
 * Проверяет наличие прав на попытку изменить статью
 *
 * Class UpdateArticle
 * @package App\Http\Requests
 */
class UpdateArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Article $article
     * @return bool
     */
    public function authorize(Article $article)
    {
        return $this->user()->can('update', $article);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
