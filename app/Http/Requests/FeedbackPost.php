<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;


/**
 * Проверяет чтоб нам не отправили всякую фигню
 *
 * Class CreateArticlePost
 * @package App\Http\Requests
 */
class FeedbackPost extends FormRequest
{
    /**
     * Отправлять письмо может каждый
     *
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'content' => 'required|max:191',
        ];
    }
}
