<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;


/**
 * Проверяет наличие прав на создание новой статьи
 * и проверяет данные если они есть
 *
 * Class CreateArticlePost
 * @package App\Http\Requests
 */
class CreateArticlePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Article $article
     * @return bool
     */
    public function authorize(Article $article)
    {
        return $this->user()->can('create', $article);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|max:100|unique:articles',
            'content' => 'required|max:255|min:6',
        ];
    }
}
