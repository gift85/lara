<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;


/**
 * Проверяет наличие прав на просмотр статьи
 *
 * Class ShowArticle
 * @package App\Http\Requests
 */
class ShowArticle extends FormRequest
{
    /**
     * Смотреть может каждый
     *
     * Determine if the user is authorized to make this request.
     *
     * @param Article $article
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
