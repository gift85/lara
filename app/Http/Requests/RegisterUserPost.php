<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;


/**
 * Проверяет наличие прав на создание новой статьи
 * и проверяет данные если они есть
 *
 * Class CreateArticlePost
 * @package App\Http\Requests
 */
class RegisterUserPost extends FormRequest
{
    /**
     * Регистрироваться может каждый
     *
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users|max:191',
            'password' => 'required|max:191|min:6',
            'password2' => 'required|same:password',
            'email' => 'required|email|unique:users|max:191',
            'phone' => [
                'nullable',
                'regex:/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/'
            ],
            'rules' => 'accepted',
        ];
    }
}
