<?php

namespace App\Http\Requests;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


/**
 * Проверяет наличие прав на изменение статьи
 * и проверяет данные если оин есть
 *
 * Class UpdateArticlePost
 * @package App\Http\Requests
 */
class UpdateArticlePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @param Article $article
     * @return bool
     */
    public function authorize(Article $article)
    {
        return $this->user()->can('update', $article);
    }

    /**
     * Здесь применено правило при котором проверяются уникальные значения в таблице
     * игнорируя текущую запись ($this->id)
     *
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100|unique:articles,title,' . $this->id,
            'content' => 'required|max:255|min:6',
        ];
    }
}
