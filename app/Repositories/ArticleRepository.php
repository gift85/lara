<?php

namespace App\Repositories;

use App\Models\Article;
use App\Repositories\Contracts\ArticleRepositoryInterface;
use Illuminate\Http\Request;


/**
 * Репозиторий работающий со статьями
 *
 * Class ArticleRepository
 * @package App\Repositories
 */
class ArticleRepository implements ArticleRepositoryInterface
{
    /** поле для модели Article */
    protected $article;

    /**
     * Внедрим модель Article
     *
     * ArticleRepository constructor.
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * {@inheritdoc}
     */
    public function getAll()
    {
        /*//todo подумать над переделкой в raw запрос (без вложенного select)
        $articles = $this->article->latest()
            ->withCount('comments')->get();//->get(['id', 'title', 'comments']); */

        $articles = $this->article->latest()
                ->withCount('comments')
                ->with('user')
                ->get();

        return $articles;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllPaginate()
    {
        /*//todo подумать над переделкой в raw запрос (без вложенного select)
        $articles = $this->article->latest()
            ->withCount('comments')->get();//->get(['id', 'title', 'comments']);*/

        $articles = $this->article->latest()
                ->withCount('comments')
                ->with('user')
                ->paginate(5);

        return $articles;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        /*
        $article = $this->article->where('articles.id', $id)
            ->join('users', 'users.id', '=', 'articles.user_id')
            ->first(['title', 'content', 'articles.created_at', 'name']);
        */
        /*//todo подумать над переделкой в raw запрос как выше
        $article = $this->article->findOrFail($id);
        where('id', $id)
            ->first(['title', 'content']);*/

        //сохранеие набора моделей в кэше, иначе выборка из базы
        $article = $this->article->with(['user', 'comments.user'])
            ->findOrFail($id);

        return $article;
    }

    /**
     * {@inheritdoc}
     */
    public function add(Article $article)
    {
        $this->article = $article;
        $this->article->save();
        return $this->article->id;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Article $article)
    {
        $this->article = $article;
        $this->article->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete($id)
    {
        $this->article->destroy($id);
    }
}