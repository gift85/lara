<?php

namespace App\Repositories\Contracts;

use App\Models\Article;

/**
 * Интерфейс для репозиториев со статьями
 *
 * Interface ArticleRepositoryInterface
 * @package App\Repositories\Contracts
 */
interface ArticleRepositoryInterface
{
    /**
     * Получение всех статей
     *
     * @return mixed
     */
    public function getAll();

    /**
     * Получение всех статей с пагинацией
     *
     * @return mixed
     */
    public function getAllPaginate();

    /**
     * Сохранение новой статьи
     *
     * @param Article $article
     * @return mixed
     */
    public function add(Article $article);

    /**
     * Обновление конкретной статьи
     *
     * @param Article $article
     * @return mixed
     */
    public function update(Article $article);

    /**
     * Удаление статьи по id
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Получение статьи по id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id);

}