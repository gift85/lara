<?php

namespace App\Repositories;

use App\Models\Article;
use App\Repositories\Contracts\ArticleRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;


/**
 * Волшебный кэширующий репозиторий для статей,
 * по факту обертка над обычным репозиторием работаеющим с бд
 *
 * Class RedisArticleRepository
 * @package App\Repositories
 */
class RedisArticleRepository implements ArticleRepositoryInterface
{
    /** @var ArticleRepository  */
    protected $articles;

    /**
     * внедрим репозиторий работающий с бд
     *
     * @param ArticleRepository $articles
     */
    public function __construct(ArticleRepository $articles)
    {
        $this->articles = $articles;
    }

    /**
     * Сохранение в кэш
     * {@inheritdoc}
     */
    public function getAll()
    {
        $articles = Cache::remember('article.all', 30, function(){
            return $this->articles->getAll();
        });

        return $articles;
    }

    /**
     * Сохранение в кэш
     * {@inheritdoc}
     */
    public function getAllPaginate()
    {
        //todo убрать манипуляции с кэшем в события (которых пока нет)
        //сохранеие набора моделей в кэше, иначе выборка из базы
        $page = Input::get('page', 1);

        $articles = Cache::remember('article.all' . $page, 30, function(){
            return $this->articles->getAllPaginate();
        });

        return $articles;
    }

    /**
     * Сохранение в кэш
     * {@inheritdoc}
     */
    public function getById($id)
    {
        //сохранеие набора моделей в кэше, иначе выборка из базы
        $article = Cache::remember("article.$id", 30, function() use ($id){
            return $this->articles->getById($id);
        });

        return $article;
    }

    /**
     * Очистка кэша
     * {@inheritdoc}
     */
    public function add(Article $article)
    {
        Cache::flush();
        return $this->articles->add($article);
    }

    /**
     * Очистка кэша
     * {@inheritdoc}
     */
    public function update(Article $article)
    {
        Cache::flush();
        $this->articles->update($article);
    }

    /**
     * Очистка кэша
     * {@inheritdoc}
     */
    public function delete($id)
    {
        Cache::flush();
        $this->articles->delete($id);
    }
}