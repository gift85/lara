<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Пока здесь слушаем отправку обратной связи
     * и формируем два письма - отправителю и нам
     *
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\GotFeedback' => [
            'App\Listeners\SendFeedback',
            'App\Listeners\SendFeedbackNotify',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
