<?php

/**
 * Поигрался с вариацией пользовательского хелпера форматирования дат
 *
 * @param     $dateTime
 * @param int $offset
 * @return mixed
 */
function formatDate($dateTime, $offset = 0){

    $class = resolve('App\Classes\FormatDate');

    return $class->getFormattedDate($dateTime, $offset);
}