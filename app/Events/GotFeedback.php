<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Мы получили новую обратную связь!
 * Запускаем при отправке формы обратной связи
 *
 * Class GotFeedback
 * @package App\Events
 */
class GotFeedback
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** от кого сообщение */
    public $emailFrom;

    /** заголовок */
    public $title;

    /** содержимое */
    public $content;

    /**
     * Create a new event instance.
     *
     * @param $emailFrom
     * @param $title
     * @param $content
     */
    public function __construct($emailFrom, $title, $content)
    {
        $this->emailFrom = $emailFrom;
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
