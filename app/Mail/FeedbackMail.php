<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Простейший mail класс
 *
 * Class FeedbackMail
 * @package App\Mail
 */
class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    /** от кого сообщение */
    public $sender;

    /** заголовок */
    public $title;

    /** содержимое */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param $sender
     * @param $title
     * @param $content
     */
    public function __construct($sender, $title, $content)
    {
        $this->sender = $sender;
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->sender)
            ->view('emails.feedback')
            ->with([
                'title' => $this->title,
                'content' => $this->content,
            ]);
    }
}
