<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Auth\Access\HandlesAuthorization;


/**
 * правила проверки доступа при манипуляциях с моделькой статей
 *
 * Class ArticlePolicy
 * @package App\Policies
 */
class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the article.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return mixed
     */
    public function view(User $user, Article $article)
    {
        return $user->role->permissions->contains('name', 'READ');
    }

    /**
     * Determine whether the user can create articles.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {

        return $user->role->permissions->contains('name', 'CREATE');
    }

    /**
     * Determine whether the user can update the article.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return mixed
     */
    public function update(User $user, Article $article)
    {
        return $user->role->permissions->contains('name', 'MODIFY');// && $user->id == $article->user_id;
    }

    /**
     * Determine whether the user can delete the article.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $article
     * @return mixed
     */
    public function delete(User $user, Article $article)
    {
        return $user->role->permissions->contains('name', 'DELETE');
    }
}
