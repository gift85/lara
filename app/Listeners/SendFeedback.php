<?php

namespace App\Listeners;

use App\Events\GotFeedback;
use App\Mail\FeedbackMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


/**
 * Слушаем отправку формы обратной связи
 * и отправляет сообщение на почту
 *
 * Class SendFeedback
 * @package App\Listeners
 */
class SendFeedback
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GotFeedback  $event
     * @return void
     */
    public function handle(GotFeedback $event)
    {
        Mail::to(config('mail.from.address'))
            ->send(new FeedbackMail($event->emailFrom, $event->title, $event->content));
    }
}
