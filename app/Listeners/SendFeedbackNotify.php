<?php

namespace App\Listeners;

use App\Events\GotFeedback;
use App\Mail\FeedbackMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


/**
 * Слушаем отправку формы обратной связи
 * и отправляет сообщение на почту пользователю
 * который нам что то написал
 *
 * Class SendFeedback
 * @package App\Listeners
 */
class SendFeedbackNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GotFeedback  $event
     * @return void
     */
    public function handle(GotFeedback $event)
    {
        Mail::to($event->emailFrom)
            ->send(new FeedbackMail(config('mail.from.address'), trans('mail.title'), trans('mail.content')));
    }
}
