<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * маршруты для работы со статьями
 */
Route::get('/', 'ArticleController@index')
    ->name('blog.main');

Route::get('/show/{id}', 'ArticleController@show')
    ->name('blog.post');

/**
 * Эти маршруты требуют авторизации
 */
Route::group(['middleware' => ['auth']], function(){
    Route::get('/create', 'ArticleController@create')
        ->name('blog.create');

    //обработка формы добавления статьи
    Route::post('/create', 'ArticleController@createPost')
        ->name('blog.create.save');

    Route::get('/edit/{id}', 'ArticleController@edit')
        ->name('blog.edit');

    //обратотка сохранения статьи после изменения
    Route::post('/edit/{id}', 'ArticleController@editPost')
        ->name('blog.edit.save');

    Route::get('/delete/{id}', 'ArticleController@delete')
        ->name('blog.delete');
});

/**
 * маршруты для авторизации и регистрации
 */
Route::get('/login', 'AuthController@login')
    ->name('login');

Route::post('/login', 'AuthController@loginPost')
    ->name('login.save');

Route::get('/logout', 'AuthController@logout')
    ->name('logout');

Route::get('/register', 'AuthController@register')
    ->name('register');

Route::post('/register', 'AuthController@registerPost')
    ->name('register.save');

/**
 * маршрут для формы обратной связи
 */
Route::get('/feedback', 'MailController@feedback')
    ->name('blog.feedback');

Route::post('/feedback', 'MailController@feedbackPost')
    ->name('blog.feedback.send');

/**
 * маршрут для смены языка
 */
Route::get('/lang/{locale}', function($locale){
    session(compact('locale'));
    return redirect()->back();
})->name('locale');


//todo маршруты для администратора

//todo маршрут к профилю пользователя, пока заглушка
Route::get('/profile/{id}', 'ArticleController@index')
    ->name('user.profile');
