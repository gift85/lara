<?php

use Faker\Generator as Faker;
use App\Models\{Article, User};

/**
 * Генератор пользователей, доступен по глобальному хелперу factory()
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Article::class, function (Faker $faker) {

    $user = User::all()->random();
    $user_id = $user->id;

    //зафиксируем дату создания пользователя чтобы дата создания статьи была не старше
    $user_created_at = $user->created_at;

    //зафиксируем дату создания для того чтобы дата обновления была не старше
    $created_at = $faker->dateTimeBetween($user_created_at, 'now');

    return [
        'user_id' => $user_id,
        'title' => $faker->company,
        'content' => $faker->realText,
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at, 'now'),
    ];
});