<?php

use Faker\Generator as Faker;
use App\Models\User;

/**
 * Генератор статей, доступен по глобальному хелперу factory()
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker $faker) {
    static $password;

    //зафиксируем дату создания для того чтобы дата обновления была не старше
    $created_at = $faker->dateTimeBetween('-3 year', 'now');

    return [
        'name' => $faker->unique()->firstName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at, 'now'),
    ];
});