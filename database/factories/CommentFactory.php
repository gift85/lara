<?php

use Faker\Generator as Faker;
use App\Models\{Comment, Article, User};

/**
 * Генератор комментариев, доступен по глобальному хелперу factory()
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Comment::class, function (Faker $faker) {

    $article = Article::all()->random();
    $article_id = $article->id;
    $article_created_at = $article->created_at;

    //дата создания комментария должна быть старше даты создания статьи
    $created_at = $faker->dateTimeBetween($article_created_at, 'now');

    //выберем автора комментария созданного ранее самого комментария
    $user = User::where('created_at', '<=', $created_at)->get()->random();
    $user_id = $user->id;

    return [
        'article_id' => $article_id,
        'user_id' => $user_id,
        'text' => $faker->realText,
        'created_at' => $created_at,
        'updated_at' => $faker->dateTimeBetween($created_at, 'now'),
    ];
});