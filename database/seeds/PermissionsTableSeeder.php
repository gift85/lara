<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;


/**
 * разрешения
 *
 * Class PermissionsTableSeeder
 */
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['name' => 'READ', 'description' => 'can see and read'],
            ['name' => 'CREATE', 'description' => 'can create new'],
            ['name' => 'MODIFY', 'description' => 'can modify'],
            ['name' => 'DELETE', 'description' => 'can delete'],
        ]);
    }
}
