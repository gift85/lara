<?php

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;


/**
 * Добавлеям 50 пользователей
 * и одного адиина
 *
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => 'Andrew',
            'email' => 'f@d.ru',
            'password' => bcrypt('123456'),
            'remember_token' => str_random(10),
            'role_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        factory(App\Models\User::class, 50)->create();
    }
}
