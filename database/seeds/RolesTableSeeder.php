<?php

use App\Models\Role;
use Illuminate\Database\Seeder;


/**
 * роли
 *
 * Class RolesTableSeeder
 */
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            ['name' => 'USER', 'description' => 'can watch'],
            ['name' => 'ADMIN', 'description' => 'can do all'],
            ['name' => 'AUTHOR', 'description' => 'can create and modify'],
            ['name' => 'MODERATOR', 'description' => 'can modify'],
        ]);
    }
}
