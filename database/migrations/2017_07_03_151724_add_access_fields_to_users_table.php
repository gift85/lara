<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Добавим поля модификаторы доступа к совершению действий
 *
 * Class AddAccessFieldsToUsersTable
 */
class AddAccessFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('can_delete')->default(0)->after('phone');
            $table->tinyInteger('can_edit')->default(0)->after('phone');
            $table->tinyInteger('can_create')->default(0)->after('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('can_create');
            $table->dropColumn('can_edit');
            $table->dropColumn('can_delete');
        });
    }
}
