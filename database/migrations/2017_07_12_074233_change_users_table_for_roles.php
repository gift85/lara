<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/**
 * Убираем простенькие поля доступа
 * добавляем поле для роли
 * со значением по умолчанию 1 - пользователь
 *
 * Class ChangeUsersTableForRoles
 */
class ChangeUsersTableForRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('can_create');
            $table->dropColumn('can_edit');
            $table->dropColumn('can_delete');
            $table->integer('role_id')->unsigned()->default(1)->after('phone');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('can_delete')->default(0)->after('phone');
            $table->tinyInteger('can_edit')->default(0)->after('phone');
            $table->tinyInteger('can_create')->default(0)->after('phone');
            $table->dropForeign(['role_id']);
            $table->dropColumn('role_id');
        });
    }
}
