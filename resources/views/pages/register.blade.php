@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.register')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'register'] )
@endsection

@section('posts')
    @include('blocks.registerform')
@endsection

