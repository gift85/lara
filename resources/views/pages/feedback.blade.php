@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.feedback')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'feedback'] )
@endsection

@section('posts')
    @include('blocks.feedback', [
        'button' => __('blog.buttons.feedback'),
        'action' => route('blog.feedback.send'),
        'title' => old('title'),
        'content' => old('content'),
    ])
@endsection

