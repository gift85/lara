@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.edit')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'edit'] )
@endsection

@section('posts')
    @include('blocks.newpost', [
        'button' => __('blog.buttons.edit'),
        'action' => route('blog.edit.save', ['id' => $article->id]),
        'title' => old('title', $article->title),
        'content' => old('content', $article->content),
        'tags' => old('tags', $article->tags),
    ])
@endsection

