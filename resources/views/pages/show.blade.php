@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.show')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'show'] )
@endsection

@section('posts')
    @include('blocks.showpost')

    @if($article->comments->count() > 0)
        @each('blocks.comment', $article->comments , 'comment')
    @else
        <p>{{ __('blog.forms.comment.empty') }}</p>
    @endif
@endsection

