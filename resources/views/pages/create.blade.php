@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.create')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'create'] )
@endsection

@section('posts')
    @include('blocks.newpost', [
        'button' => __('blog.buttons.save'),
        'action' => route('blog.create.save'),
        'title' => old('title'),
        'content' => old('content'),
        'tags' => old('tags'),
    ])
@endsection

