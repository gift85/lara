@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.login')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'login'] )
@endsection

@section('posts')
    @include('blocks.loginform')
@endsection

