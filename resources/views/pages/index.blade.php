@extends('layouts.newbase')

@section('head')
    @include('blocks.head', ['title' => __('head.title.index')])
@endsection

@section('menu')
    @include('blocks.menu', ['name' => 'index'] )
@endsection

@section('posts')
    @each('blocks.post', $articles, 'article')
@endsection

@section('pagination')
    <!-- Pagination -->
    <ul class="actions pagination">
        <li><a href="{{ $articles->previousPageUrl() }}" class="{{ $articles->previousPageUrl() ? : 'disabled' }} button big previous">{{ __('pagination.previous') }}</a></li>
        <li><a href="{{ $articles->nextPageUrl() }}" class="button big next">{{ __('pagination.next') }}</a></li>
    </ul>
@endsection
