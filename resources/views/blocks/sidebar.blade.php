<!-- Sidebar -->
<section id="sidebar">
    <!-- Intro -->
    <section id="intro">
        <a href="#" class="logo"><img src="/images/logo.jpg" alt="" /></a>
        <header>
            <h2>{{ Auth::user()->name ?? __('blog.guest') }}</h2>
            <p>{{ __('blog.stub') }}</p>
        </header>
    </section>

    <!-- Mini Posts -->
    <section>
        <div class="mini-posts">

            <!-- Mini Post -->
            <article class="mini-post">
                <header>
                    <h3><a href="#">{{ __('blog.stub') }}</a></h3>
                    <time class="published" datetime="2015-10-17">{{ __('blog.stub') }}</time>
                    <a href="#" class="author"><img src="/images/avatar.jpg" alt="" /></a>
                </header>
                <a href="#" class="image"><img src="/images/pic07.jpg" alt="" /></a>
            </article>

        </div>
    </section>

    <!-- Posts List -->
    <section>
        <ul class="posts">
            <li>
                <article>
                    <header>
                        <h3><a href="#">{{ __('blog.stub') }}</a></h3>
                        <time class="published" datetime="2015-10-20">{{ __('blog.stub') }}</time>
                    </header>
                    <a href="#" class="image"><img src="/images/pic08.jpg" alt="" /></a>
                </article>
            </li>
        </ul>
    </section>

    <!-- About -->
    <section class="blurb">
        <h2>{{ __('blog.about.title') }}</h2>
        <p>{{ __('blog.about.text') }}</p>
        <ul class="actions">
            <li><a href="#" class="button">{{ __('blog.about.more') }}</a></li>
        </ul>
    </section>

    <!-- Footer -->
    <section id="footer">
        <ul class="icons">
            <li><a href="#" class="fa-twitter"><span class="label">Twitter</span></a></li>
            <li><a href="#" class="fa-facebook"><span class="label">Facebook</span></a></li>
            <li><a href="#" class="fa-instagram"><span class="label">Instagram</span></a></li>
            <li><a href="#" class="fa-rss"><span class="label">RSS</span></a></li>
            <li><a href="#" class="fa-envelope"><span class="label">Email</span></a></li>
        </ul>
        <p class="copyright">&copy; Untitled. Design: <a href="http://html5up.net">HTML5 UP</a>. Images: <a href="http://unsplash.com">Unsplash</a>.</p>
    </section>
</section>