<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- заглушка под заголовок вкладки --}}
    <title>{{ $title }}</title>

    {{-- заглушка под внедрение нужных стилей --}}
    @include('blocks.styles.common')
</head>
