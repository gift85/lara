@include('blocks.menu.parts.index')
{{-- доступ к пунктам ниже должен ограничиваться политиками,
в частности на эту страницу нельзя зайти без прав --}}
@include('blocks.menu.parts.show')
@include('blocks.menu.parts.delete')