<li>
    <a href="{{ route('login') }}">
        <h3>{{ __('links.login.text') }}</h3>
        <p>{{ __('links.login.description') }}</p>
    </a>
</li>
