<li>
    <a href="{{ route('register') }}">
        <h3>{{ __('links.register.text') }}</h3>
        <p>{{ __('links.register.description') }}</p>
    </a>
</li>
