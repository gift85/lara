<li>
    <a href="{{ route('blog.edit', ['id' => $id]) }}">
        <h3>{{ __('links.edit.text') }}</h3>
        <p>{{ __('links.edit.description') }}</p>
    </a>
</li>
