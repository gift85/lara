<li>
    <a href="{{ route('blog.create') }}">
        <h3>{{ __('links.create.text') }}</h3>
        <p>{{ __('links.create.description') }}</p>
    </a>
</li>
