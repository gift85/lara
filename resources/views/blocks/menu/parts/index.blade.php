<li>
    <a href="{{ route('blog.main') }}">
        <h3>{{ __('links.index.text') }}</h3>
        <p>{{ __('links.index.description') }}</p>
    </a>
</li>
