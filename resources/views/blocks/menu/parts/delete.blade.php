<li>
    <a href="{{ route('blog.delete', ['id' => $id]) }}">
        <h3>{{ __('links.delete.text') }}</h3>
        <p>{{ __('links.delete.description') }}</p>
    </a>
</li>
