<li>
    <a href="{{ route('blog.post', ['id' => $id]) }}">
        <h3>{{ __('links.show.text') }}</h3>
        <p>{{ __('links.show.description') }}</p>
    </a>
</li>
