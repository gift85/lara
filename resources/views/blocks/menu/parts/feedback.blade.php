<li>
    <a href="{{ route('blog.feedback') }}">
        <h3>{{ __('links.feedback.text') }}</h3>
        <p>{{ __('links.feedback.description') }}</p>
    </a>
</li>
