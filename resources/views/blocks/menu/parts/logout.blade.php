<li>
    <a href="{{ route('logout') }}">
        <h3>{{ __('links.logout.text') }}</h3>
        <p>{{ __('links.logout.description') }}</p>
    </a>
</li>
