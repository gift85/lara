@include('blocks.menu.parts.index')
@if(Auth::Check())
    @include('blocks.menu.parts.edit')
    @include('blocks.menu.parts.delete')
@endif