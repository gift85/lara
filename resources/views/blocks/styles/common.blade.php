{{-- общий стиль по умолчанию с возможностью выбора
 выбор происходит в вью композере методе boot хелпер сервиса,
  а так же при переходе по маршруту /theme/{id}
<link href="/css/style{{ $currentTheme or 1 }}.css" rel="stylesheet">
на пробу нового шаблона отключил
--}}

<!--[if lte IE 8]><script src="{{ asset('js/ie/html5shiv.js') }}"></script><![endif]-->
<link rel="stylesheet" href="{{ asset('css/main.css') }}" />
<!--[if lte IE 9]><link rel="stylesheet" href="{{ asset('css/ie9.css') }}" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('css/ie8.css') }}" /><![endif]-->