<!-- Menu -->
<section id="menu">

    <!-- Search -->
    <section>
        <form class="search" method="get" action="#">
            <input type="text" name="query" placeholder="{{ __('header.search') }}" />
        </form>
    </section>

    <!-- Links -->
    <section>
        <ul class="links">
            @include("blocks.menu.$name")
        </ul>
    </section>

    <!-- Actions -->
    <section>
        <ul class="actions vertical">
            @if(Auth::Check())
                <li><a href="{{ route('logout') }}" class="button big fit">{{ __('links.logout.text') }}</a></li>
            @else
                <li><a href="{{ route('register') }}" class="button big fit">{{ __('links.register.text') }}</a></li>
                <li><a href="{{ route('login') }}" class="button big fit">{{ __('links.login.text') }}</a></li>
            @endif
        </ul>
    </section>

</section>