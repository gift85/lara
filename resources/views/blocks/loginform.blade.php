<!-- Post -->
<article class="post new">
    <form id="login" method="post" action="{{ route('login.save') }}">
        {{ csrf_field() }}
        @if(session('authError'))
            <div>
                {{ __(session('authError')) }}
            </div>
        @endif
        <label for="name">{{ __('blog.forms.login.name') }}</label>
        <input type="text" id="name" name="name" value="{{ old('name') }}">
        <label for="password">{{ __('blog.forms.login.password') }}</label>
        <input type="password" id="password" name="password"><br>
        <input type="checkbox" id="remember" name="remember">
        <label for="remember">{{ __('blog.forms.login.remember') }}</label><br>
        <button>{{ __('blog.buttons.login') }}</button>
    </form>
</article>