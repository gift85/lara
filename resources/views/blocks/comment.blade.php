<article class="post new">
    <header>
        <div class="title">
            <h3>{{ __('blog.forms.comment.by') }}<a href="{{ route('user.profile', $comment->user->id) }}" class="author"><span class="name">{{ $comment->user->name }}</span><img src="/images/avatar.jpg" alt="" /></a></h3>
            <p>{{ __('blog.forms.comment.at') }} {{ formatDate($comment->created_at) }}</p>
        </div>
    </header>
    <footer>
        <p>{{ $comment->text }}</p>
    </footer>
</article>