<!-- Header -->
<header id="header">
    <h1><a href="#">{{ __('links.chooselang') }}</a></h1>
    <nav class="links">
        @include('blocks.headerlinks')
    </nav>
    <nav class="main">
        <ul>
            <li class="search">
                <a class="fa-search" href="#search">{{ __('header.search') }}</a>
                <form id="search" method="get" action="#">
                    <input type="text" name="query" placeholder="{{ __('header.search') }}" />
                </form>
            </li>
            <li class="menu">
                <a class="fa-bars" href="#menu">{{ __('header.menu') }}</a>
            </li>
        </ul>
    </nav>
</header>