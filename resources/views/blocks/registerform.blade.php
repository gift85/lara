<!-- Post -->
<article class="post new">
    <form id="register" method="post" action="{{ route('register.save') }}">
        {{ csrf_field() }}
        <label for="name">{{ __('blog.forms.login.name') }}</label>
        {{ $errors->first('name') ?? '' }}
        <input type="text" id="name" name="name" value="{{ old('name') }}">
        <label for="password">{{ __('blog.forms.login.password') }}</label>
        {{ $errors->first('password') ?? '' }}
        <input type="password" id="password" name="password">
        <label for="password2">{{ __('blog.forms.register.password') }}</label>
        {{ $errors->first('password2') ?? '' }}
        <input type="password" id="password2" name="password2">
        <label for="email">{{ __('blog.forms.register.email') }}</label>
        {{ $errors->first('email') ?? '' }}
        <input type="email" id="email" name="email" value="{{ old('email') }}">
        <label for="phone">{{ __('blog.forms.register.phone') }}</label>
        {{ $errors->first('phone') ?? '' }}
        <input type="text" id="phone" name="phone" value="{{ old('phone') }}">
        <input type="checkbox" id="rules" name="rules">
        <label for="rules">{{ __('blog.forms.register.rules') }}</label><br>
        {{ $errors->first('rules') ?? '' }}<br>
        <button>{{ __('blog.buttons.register') }}</button>
    </form>
</article>