<!-- Post -->
<article class="post new">
    <form id="newpost" method="post" action="{{ $action }}">
        {{ csrf_field() }}
        <label for="title">{{ __('blog.forms.create.title') }}</label>
        {{ $errors->first('title') ?? '' }}
        <input type="text" size="60" id="title" name="title" value="{{ $title }}">
        <label for="tags">{{ __('blog.forms.create.tags') }}</label>
        {{ $errors->first('tags') ?? '' }}
        <input type="text" size="60" id="tags" name="tags" value="{{ $tags }}">
        {{ $errors->first('content') ?? '' }}
        <label for="content">{{ __('blog.forms.create.content') }}</label>
        <textarea cols="120" rows="15" id="content" name="content">{{ $content }}</textarea><br>
        <button>{{ __($button) }}</button>
    </form>
</article>