<!-- Post -->
<article class="post">
    <header>
        <div class="title">
            <h2><a href="{{ route('blog.post', ['id' => $article->id]) }}">{{ $article->title }}</a></h2>
            <p>{{ __('blog.stub') }}</p>
        </div>
        <div class="meta">
            <time class="published" datetime="2015-11-01">{{ formatDate($article->created_at) }}</time>
            <a href="{{ route('user.profile', $article->user->id) }}" class="author"><span class="name">{{ $article->user->name }}</span><img src="/images/avatar.jpg" alt="" /></a>
        </div>
    </header>
    <a href="{{ route('blog.post', ['id' => $article->id]) }}" class="image featured"><img src="/images/pic01.jpg" alt="" /></a>
    <p>{{ $article->content }}</p>
    <footer>
        <ul class="actions">
            <li><a href="{{ route('blog.post', ['id' => $article->id]) }}" class="button big">{{ __('blog.continue') }}</a></li>
        </ul>
        <ul class="stats">
            <li><a href="#">{{ __('blog.general') }}</a></li>
            <li><a href="#" class="icon fa-heart">0</a></li>
            <li><a href="#" class="icon fa-comment">{{ $article->comments_count }}</a></li>
        </ul>
    </footer>
</article>