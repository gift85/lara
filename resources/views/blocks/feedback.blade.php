<!-- Post -->
<article class="post new">
    <form id="newpost" method="post" action="{{ $action }}">
        {{ csrf_field() }}
        <label for="title">{{ __('blog.forms.feedback.title') }}</label><br>
        {{ $errors->first('title') ?? '' }}<br>
        <input type="text" size="60" id="title" name="title" value="{{ $title }}"><br>
        {{ $errors->first('content') ?? '' }}<br>
        <label for="content">{{ __('blog.forms.feedback.content') }}</label><br>
        <textarea cols="120" rows="10" id="content" name="content">{{ $content }}</textarea><br>
        <button>{{ __($button) }}</button>
    </form>
</article>