@extends('layouts.newblog')

@section('header')
    @include('blocks.header')
@endsection

@section('sidebar')
    @include('blocks.sidebar')
@endsection

@section('scripts')
    @include('blocks.scripts')
@endsection
