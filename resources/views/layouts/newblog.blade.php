<!doctype html>
<html lang="{{ config('app.locale') }}">
    @yield('head')
    <body>
        <!-- Wrapper -->
        <div id="wrapper">
            @yield('header')
            @yield('menu')
                <!-- Main -->
                <div id="main">
                    @yield('posts')
                    @yield('pagination')
                </div>
            @yield('sidebar')
        </div>
        @yield('scripts')
    </body>
</html>
