<?php

return [
    'chooselang' => 'Choose language',
    'ru' => 'Russian',
    'en' => 'English',
    'create' => [
        'text' => 'Create',
        'description' => 'Create new article',
    ],
    'logout' => [
        'text' => 'Logout',
        'description' => 'Just log out',
    ],
    'login' => [
        'text' => 'Login',
        'description' => 'Just log in',
    ],
    'register' => [
        'text' => 'Register',
        'description' => 'Create new profile',
    ],
    'feedback' => [
        'text' => 'Feedback',
        'description' => 'Tell if something wrong',
    ],
    'show' => [
        'text' => 'Back to view this article',
        'description' => 'Changes will not be saved',
    ],
    'index' => [
        'text' => 'Go to main page',
        'description' => 'List all articles',
    ],
    'edit' => [
        'text' => 'Edit',
        'description' => 'Edit this article',
    ],
    'delete' => [
        'text' => 'Delete',
        'description' => 'Delete this article',
    ],
];