<?php

return [
    'stub' => 'Stub',
    'guest' => 'Guest',
    'logerror' => 'I don\'t see that user',
    'continue' => 'Continue reading',
    'general' => 'General',
    'about' => [
        'title' => 'About',
        'text' => 'Academic blog project',
        'more' => 'Learn more',
        ],
    'buttons' => [
        'save' => 'Save',
        'edit' => 'Change',
        'feedback' => 'Send',
        'login' => 'Log in',
        'register' => 'Register',
    ],
    'forms' => [
        'create' => [
            'title' => 'Title',
            'content' => 'Content',
            'tags' => 'Tags',
        ],
        'feedback' => [
            'title' => 'Title',
            'content' => 'Message',
        ],
        'comment' => [
            'by' => 'Author:',
            'at' => 'at:',
            'empty' => 'No comments yet',
        ],
        'login' => [
            'name' => 'Your name',
            'password' => 'Password',
            'remember' => 'Remember?',
        ],
        'register' => [
            'password' => 'Password once again',
            'email' => 'E-mail',
            'phone' => 'Phone number (not required)',
            'rules' => 'I agree with rules',
        ],
    ],
    'errors' => [
        'title' => 'Error',
        '404' => 'Page not found',
        '500' => 'Internal server error',
        '403' => 'No access',
    ],
];