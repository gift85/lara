<?php

return [
    'months' => [
        'j1anuary',
        'f1ebruary',
        'm1arch',
        'a1pril',
        'm1ay',
        'j1une',
        'j1uly',
        'a1ugust',
        's1eptember',
        'o1ctober',
        'n1ovember',
        'd1ecember',
    ],
    'days' => [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wensday',
        'Thurday',
        'Friday',
        'Saturday',
    ],

    /*
     * если формат оставить как в русской версии,
     * с месяцами как заглушки,
     *  то получается какой то кабздец
     */
    'dateformat' => 'l, F d Y H:i',
];