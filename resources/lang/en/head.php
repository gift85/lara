<?php

return [
    'notitle' => 'No title',
    'title' => [
        'index' => 'Main page',
        'show' => 'Article',
        'create' => 'New article',
        'edit' => 'Edit article',
        'login' => 'Login',
        'feedback' => 'Feedback',
        'register' => 'Register',
    ],
];