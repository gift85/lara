<?php

return [
    'stub' => 'Заглушка',
    'guest' => 'Гость',
    'logerror' => 'Такого пользователя нет',
    'continue' => 'Продолжить чтение',
    'general' => 'основное?',
    'about' => [
        'title' => 'О блоге',
        'text' => 'Учебный блогопроект',
        'more' => 'Узнать больше',
        ],
    'buttons' => [
        'save' => 'Сохранить',
        'edit' => 'Изменить',
        'feedback' => 'Отправить',
        'login' => 'Войти',
        'register' => 'Зарегистрироваться',
    ],
    'forms' => [
        'create' => [
            'title' => 'Заголовок статьи',
            'content' => 'Содержимое',
            'tags' => 'Тэги',
        ],
        'feedback' => [
            'title' => 'Заголовок сообщения',
            'content' => 'Что изволишь донести',
        ],
        'comment' => [
            'by' => 'Автор комментария:',
            'at' => 'оставлен:',
            'empty' => 'Пока нет комментариев',
        ],
        'login' => [
            'name' => 'Твое имя',
            'password' => 'Пароль',
            'remember' => 'Запомнить?',
        ],
        'register' => [
            'password' => 'Пароль еще раз',
            'email' => 'Электронный адрес',
            'phone' => 'Номер телефона (не обязательно)',
            'rules' => 'Согласен с правилами',
        ],
    ],
    'errors' => [
        'title' => 'Ошибка',
        '404' => 'Страница не найдена',
        '500' => 'Внутренняя ошибка сервера',
        '403' => 'Доступ запрещен',
    ],
];